# Palo Alto
> This repo will showcase examples of communicating to a palo alto device via API calls.

## Setup
---
- This project uses a `.env` file to load variables, create one following this example
    ```
    username = 'myusername'
    password = 'mypassword'
    device_ip = 10.11.1.28
    ```
- Setup python environment
    ```
    python -m venv venv
    source venv/bin/activate

    pip install -r requirements
    ```

## Scripts
---
- `main.py` - Simple python script that houses functions for interacting with palo alto api
  - Note : Pay attention to functions using the RestAPI and the ones using XML API.

- `palo_alto.postman_collection.json` - Postman collection for api calls

## References
---
- https://knowledgebase.paloaltonetworks.com/KCSArticleDetail?id=kA10g000000ClN7CAK
