import requests
from requests import api
import xmltodict
from dotenv import load_dotenv
import os
import json
requests.packages.urllib3.disable_warnings()


def get_api_key(device_ip, username, password) -> str:
    '''
    Use XML Api to generate authentication key
    Params:
      device_ip (str) : ip address of the target device
      username (str) : username for palo alto device
      password (str) : password for palo alto device

    Returns : api_key string
    '''
    params = (
        ('type', 'keygen'),
        ('user', username),
        ('password', password),
    )

    response = requests.get(
        f'https://{device_ip}/api/', params=params, verify=False)

    if response.status_code == 200:
        dict_data = xmltodict.parse(response.content)
        return dict_data['response']['result']['key']
    else:
        return 'Unable to get api key'

# function using xml api


def get_interface_info(device_ip, api_key, interface="management"):
    '''
    Uses the xml api to retrieve interface information
    Params:
      device_ip (str) : ip address of the target device
      api_key (str) : api key authenticated with target device
      interface (str) : interface to gather information about, default = management
    '''

    params = (
        ('type', 'op'),
        ('cmd', f'<show><interface>{interface}</interface></show>'),
    )
    headers = {
        "X-PAN-KEY": api_key,
    }

    response = requests.get(
        f'https://{device_ip}/api/', headers=headers, params=params, verify=False)

    dict_data = xmltodict.parse(response.content)
    interface_data = dict_data['response']['result']['info']
    return interface_data


# function using rest API
def get_ethernet_interfaces(device_ip, api_key) -> list:
    '''
    Using the rest API to return all configured ethernet interfaces
    Params:
      device_ip (str) : ip address of the target device
      api_key (str) : api key authenticated with target device
    Returns : List of objects containiner interface information
    '''

    headers = {
        'X-PAN-KEY': api_key,
        'Accept': 'application/json'
    }

    response = requests.get(
        f"https://{device_ip}/restapi/v10.0/Network/EthernetInterfaces", headers=headers, verify=False)

    interfaces = response.json()['result']['entry']

    return interfaces


# function using rest API
def update_ethernet_interface(device_ip, api_key, interface_name, payload):
    '''
    Use Rest API to update an ethernet interface with desired payload
    Params:
      device_ip (str) : ip address of the target device
      api_key (str) : api key authenticated with target device
      interface_name (str) : name of interface to update
      paylod (dict) : dict of payload to update interface with

    Example Payload : 
     {
        "@name": "ethernet1/2",
        "layer2": {
            "lldp": {
                "enable": "no"
            }
    }}

    Returns requests.models.Response object
    '''
    headers = {
        'X-PAN-KEY': api_key,
        'Accept': 'application/json'
    }

    params = (
        ('name', interface_name),
    )

    response = requests.put(
        f"https://{device_ip}/restapi/v10.0/Network/EthernetInterfaces", headers=headers, params=params, verify=False, data=json.dumps({"entry": payload}))

    return response.json()


def get_vlans_info(device_ip, api_key) -> list:
    '''
    Uses RestAPI to retrieve vlans from palo alto device
    Params:
      device_ip (str) : ip address of the target device
      api_key (str) : api key authenticated with target device

    Return (list) : objects for each vlan
    '''

    headers = {
        'X-PAN-KEY': api_key,
        'Accept': 'application/json'
    }

    response = requests.get(
        f"https://{device_ip}/restapi/v10.0/Network/VLANs", headers=headers, verify=False)

    return response.json()['result']['entry']


if __name__ == "__main__":

    # load env variables
    load_dotenv()
    username = os.getenv('username')
    password = os.getenv('password')
    device_ip = os.getenv('device_ip')

    # grab an api key
    api_key = get_api_key(device_ip, username, password)

    # get mgmt interface information
    mgmt_interface_info = dict(get_interface_info(
        device_ip, api_key, interface="management"))
    print('Management Interface : ', json.dumps(mgmt_interface_info, indent=4))

    # get ethernet interfaces
    ethernet_interfaces = get_ethernet_interfaces(device_ip, api_key)
    print('Ethernet Interfaces : ', json.dumps(ethernet_interfaces, indent=4))

    # update ethernet interface
    # example of current
    '''
    {
        "@name": "ethernet1/2",
        "virtual-wire": {
            "lldp": {
                "enable": "no"
            }
        }
    }
    '''
    updated_interface = {
        "@name": "ethernet1/2",
        "layer2": {
            "lldp": {
                "enable": "no"
            }
        }}

    # uncomment below to test updating interface
    # res = update_ethernet_interface(
    #     device_ip=device_ip, api_key=api_key, interface_name="ethernet1/2", payload=updated_interface)

    # Gather Vlan information
    vlan_info = get_vlans_info(device_ip, api_key)
    print('Vlan Info : ', json.dumps(vlan_info, indent=4))
